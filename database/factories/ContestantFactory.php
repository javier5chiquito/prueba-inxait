<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contestant;
use Faker\Generator as Faker;

$factory->define(Contestant::class, function (Faker $faker) {

    return [
    	"name"=> factory(App\Contestant::class),
		"last_name"=>$faker->name,
		"identification"=>'123456789',
		"id_department"=>$faker->id_department,
		"id_city"=>$faker->id_city,
		"phone"=>$faker->phone,
		"email"=>$faker->unique()->safeEmail,
		"habeas"=>true 
	];
});
