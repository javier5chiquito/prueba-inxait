<?php

use Illuminate\Database\Seeder;
use App\Contestant;

class ContestantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
 	public function run()
    {
        factory(Contestant::class)->times(3)->create();
    }
}
