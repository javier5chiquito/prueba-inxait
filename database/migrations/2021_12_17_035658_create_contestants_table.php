<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContestantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contestants', function (Blueprint $table) {
            $table->id();
            $table->string('name',100);
            $table->string('last_name',100)->nullable();
            $table->string('identification',20)->unique();
            $table->integer('id_department');
            $table->integer('id_city');
            $table->string('phone',20)->nullable();
            $table->string('email',200)->unique();
            $table->boolean('habeas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contestants');
    }
}
