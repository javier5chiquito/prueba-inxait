<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/home', 'CoursesController@index')->name('home');
Route::get('/', 'HomeController@index');

// Rutas de operadores 
Route::get('/get_citys/{id}', 'HomeController@getCitys');
Route::get('register', 'HomeController@register');
Route::post('/register', 'HomeController@postRegister')->name('register');
Route::get('/export_excell', 'HomeController@exportExcell');
