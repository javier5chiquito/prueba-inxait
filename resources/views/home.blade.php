@extends('layouts.app')

@section('content')
<div class="container">
  <!-- Start your project here-->

    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
        <a href="" class="navbar-brand">LOGO</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#is">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse " id="is">
            <div class="navbar-nav w-100 justify-content-lg-center flex-md-wrap">
                <div class="nav-item">
                    <a href="{{url('register')}}" class="nav-link">Registrarse</a>
                </div>
                <div class="nav-item">
                    <a href="{{url('export_excell')}}" class="nav-link">Ver registros</a>
                </div>
            </div>
        </div>
    </nav>


    <div id="bannerwin" class="bannerwin">
        <h5>@if($win != []) El ganador es : <br> 
                            " {{$win->name}} {{$win->last_name}} <br>
                            C.C : {{$win->identification}} " @else " Aqui se anuncia al ganador " @endif</h5>
    </div>

    <div class="container jumbotron clearfix my-lg-4">
       <h1 class="text-center">Concurso Promocional</h1>
        <div class="row no-gutters p-md-5 p-2">

            <div class="col-md-4 col-12 px-2">
                <div class="text-center"><i><span class="fa fa-user fa-5x"></span></i></div>
                <p class="mt-3">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci architecto
                consequatur cum eum laborum magni nam necessitatibus nisi? Ad cumque debitis
                dolore eum hic laboriosam nesciunt odit officia omnis similique.
        
                </p>
                <img style="width: 100%;" src="https://i0.wp.com/www.oinkoink.com.mx/wp-content/uploads/2019/06/devaluacion-autos-salen-agencia-me%CC%81xico.png?fit=836%2C469&ssl=1">
            </div>
            <div class="col-md-4 col-12 px-2">
                <div class="text-center"><i><span class="fa fa-industry fa-5x"></span></i></div>
                <p class="mt-3">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci architecto
                consequatur cum eum laborum magni nam necessitatibus nisi? Ad cumque debitis
                dolore eum hic laboriosam nesciunt odit officia omnis similique.
               </p>
                <img style="width: 100%;" src="https://www.megautos.com/wp-content/uploads/2020/05/agencia-de-autos-usados-en-venta-1024x669.jpg">
            </div>
            <div class="col-md-4 col-12 px-2">
                <div class="text-center"><i><span class="fa fa-dashboard fa-5x"></span></i></div>
                 <p class="mt-3">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci architecto
                consequatur cum eum laborum magni nam necessitatibus nisi? Ad cumque debitis
                dolore eum hic laboriosam nesciunt odit officia omnis similique.
                </p>
                <img style="width: 100%;" src="https://www.govirtual.com.mx/hubfs/Blog_Julio_18/16_JULIO_POR_QUE_ES_IMPORTANTE_MANTENER_ACTUALIZADO_TU_INVENTARIO_DE_AUTOS.jpg">
            </div>
        </div>

        <div class="col-md-12 col-12 px-2" style="text-align: center;">
           <p>Para registrarte llena el siguiente formulario.</p>
           <br><a href="">Registrate Aqui</a>
        </div>


    </div>

         <div class="row" style=" background: #3674a7; color: white;">
          <div class="col-12 col-md-7">

          </div>
          <div class="col-12 col-md-5 p-5">
              <address class="m-lg-12">
                      <p>Javier Chiquito Bogota, Colombia</p>
                      <p>2021 jch13</p>
                  </address>
          </div>
      </div>
    <!-- /Start your project here-->
    
</div>
@endsection
