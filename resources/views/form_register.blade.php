@extends('layouts.app')
@section('content')

<div class="container">
<div class="col-md-5">
    <div class="form-area">
        @if (session()->has('mensaje'))
                <div class="error">
                    <p class="bg-info msj-info"> {{ session('mensaje') }} </p>
                </div>

                    {!! session()->forget('mensaje') !!}
        @endif

        <form role="form" id="form_register" method="POST" action="{{route('register')}}">
            @csrf
        <br style="clear:both">
                    <h3 style="margin-bottom: 25px; text-align: center;">Formulario de inscripcion</h3>
    				<div class="form-group">
						<input type="text" class="form-control sol-let" id="name" name="name" placeholder="Nombres" required>

                        @if ($errors->has('name'))
                            <small class="form-text text-danger">{{ $errors->first('name') }}</small>
                        @endif
					</div>
                    <div class="form-group">
                        <input type="text" class="form-control sol-let" id="last_name" name="last_name" placeholder="Apellidos">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control sol-num" id="identification" name="identification" placeholder="Cédula" required>

                        @if ($errors->has('identification'))
                            <small class="form-text text-danger">{{ $errors->first('identification') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <select class="form-control" data-live-search="true" name="department" id="department" required>
                            <option value="">...................</option>
                            @foreach($department as $item)
                              <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('department'))
                            <small class="form-text text-danger">{{ $errors->first('department') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <select class="form-control" data-live-search="true" name="city" id="city" required>
                            <option value="">..</option>
                        </select>
                        @if ($errors->has('city'))
                            <small class="form-text text-danger">{{ $errors->first('city') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control sol-num" id="phone" name="phone" placeholder="celular" required>

                        @if ($errors->has('phone'))
                            <small class="form-text text-danger">{{ $errors->first('phone') }}</small>
                        @endif
                    </div>

					<div class="form-group">
						<input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                        @if ($errors->has('email'))
                            <small class="form-text text-danger">{{ $errors->first('email') }}</small>
                        @endif
					</div>
                    <div class="form-group">
                        <label for="customRadio"><input name="habeas" value="1" type="radio">
                        Autorizo el tratamiento de mis datos de acuerdo con la
                        finalidad establecida en la política de protección de datos personales. Haga clic aqui</label>
                        @if ($errors->has('habeas'))
                         <small class="form-text text-danger">{{ $errors->first('habeas') }}</small>
                        @endif             
                    </div>
            
                    <div class="form-group">
                      <a href="{{url('/')}}" class="btn btn-danger float-left">Regresar</a>
                    </div>
                    <button type="submit" id="submit" name="submit" class="btn btn-primary float-right">Guardar</button>

        </form>
        
    </div>
</div>
</div>

@endsection
