<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Contestant extends Model
{
    //
    public function scopeAllContestant($query)
    {
        $data = DB::table('contestants')
                  ->orderBy('id','desc')
                  ->paginate(5);

        return $data;
    }

    public function scopeCountContestant($query)
    {
        $data = DB::table('contestants')
        		  ->count();

        return $data;
    }
}
