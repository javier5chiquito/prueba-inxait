<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contestant;
use App\City;
use App\Department;
use App\Exports\ContestantsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\ValidarFormularioRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       $win = $this->calculateWin();
       return view('home',compact('win'));
    }

    public function register()
    {
        
        $department = Department::AllDepartment();
        return view('form_register',compact('department'));
    }

    public function postRegister(ValidarFormularioRequest $request)
    {
        $candidate = new Contestant;
        $candidate->name = $request['name'];
        $candidate->last_name = $request['last_name'];
        $candidate->identification = $request['identification'];
        $candidate->id_department = $request['department'];
        $candidate->id_city = $request['city'];
        $candidate->phone = $request['phone'];
        $candidate->email = $request['email'];
        $candidate->habeas = (isset($request['habeas']))?true:false;
        $candidate->save();

      return redirect()->back()->with('mensaje', 'Se ha guardado correctamente');
    }

    public function getCitys($id)
    {
        $citys = City::select('id','name')->where('department_id',$id)->get()->toArray();

       return response()->json(['status' => 200,'data' => $citys]);
    }

    public function exportExcell()
    {
        return Excel::download(new ContestantsExport, 'Concursantes.xlsx');
    }

    public function calculateWin()
    {
       $consult = Contestant::CountContestant();
        if(isset($consult)){

           if($consult >= 5){
            $winner = Contestant::all()->random(1)->first();
            return $winner;
           }
        }

        return [];
    }
}
