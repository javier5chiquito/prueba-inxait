<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidarFormularioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'name'            => 'required|min:1',
        'identification'  => 'required|integer|min:1|unique:contestants,identification',
        'phone'           => 'required',
        'department'      => 'required|integer',
        'city'            => 'required|integer',
        'email'           => 'required|email|unique:contestants,email',
        'habeas'          => 'required',
        ];
    }

    public function messages()
{
    return [
        'name.required'   => 'El nombre es obligatorio.',
        'name.min'        => 'El nombre debe contener mas de una letra.',

        'identification.required'   => 'El campo identificacion es obligatorio.',
        'identification.integer'    => 'El :attribute debe ser entero.',
        'identification.unique'       => 'El numero de identificacion ya esta registrado.',

        'phone.required'   => 'El campo celular es obligatorio.',

        'department.required'    => 'El departamento es obligatorio.',
        'city.required'    => 'El campo ciudad es obligatorio.',

        'email.required'    => 'El :attribute es obligatorio.',
        'email.email'       => 'El :attribute debe ser un correo válido.',
        'email.unique'       => 'El :attribute ya esta registrado.',

        'habeas.required'   => 'El campo es obligatorio.'
    ];
}

}
