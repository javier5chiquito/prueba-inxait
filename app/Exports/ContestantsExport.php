<?php

namespace App\Exports;

use App\Contestant;
use App\City;
use App\Department;
use Maatwebsite\Excel\Concerns\FromCollection;

class ContestantsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Contestant::join('cities', 'cities.id', '=', 'contestants.id_city')
            ->join('departments', 'departments.id', '=', 'contestants.id_department')
            ->select('contestants.name', 'contestants.last_name','contestants.phone', 'contestants.email',
                      'cities.name AS city', 'departments.name AS department')->get();
    }
}
