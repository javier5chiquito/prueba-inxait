<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class City extends Model
{
    //
    public function scopeAllCity($query)
    {
        $data = DB::table('Cities')
                  ->orderBy('id','desc')
                  ->paginate(5);

        return $data;
    }
}
