<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Department extends Model
{
    //
    public function scopeAllDepartment($query)
    {
        $data = DB::table('departments')
        		  ->select('id','name')
                  ->orderBy('id','desc')
                  ->get();

        return $data;
    }
}
